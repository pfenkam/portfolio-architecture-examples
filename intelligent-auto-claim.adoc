= Intelligent Automation Workflow for Claims
Christina Lin @Christina_wm, Thalia Hooker
:homepage: https://gitlab.com/osspa/portfolio-architecture-examples
:imagesdir: images
:icons: font
:source-highlighter: prettify
:toc: left
:toclevels: 5
:hardbreaks:

Insurance companies now increasingly depend on digital technology to provide customers with a satisfying and faster claim experience. As digitalization in the technology world accelerates, Insurance companies found themselves relying on old legacy systems and processes, which drive up the cost and resources to maintain the redundant systems they accumulate throughout the years. To achieve the full benefits of digitalization requires real-time data access via open and secured interfaces. Introducing artificial intelligence and automation can reduce manual involvement that leads to a smooth workload. Modernizing a legacy IT platform can be difficult. 

====
*Why Intelligent Automation Workflow ?*

. Streamlined processing to deliver  hassle-free, seamless, and accurate customer claim experience
. Enhanced fraud detection and prevention, reduced skills required for associates and limits the amount of manual work.
. Improved operational efficiency, and simplify workflow automation to reduce ongoing maintenance cost.
====

*Use case:* The architecture is based on a study of claims systems in insurance companies that automate existing manual processes, tasks, and workflows successfully by introducing Robotic Process Automation, AI/ML to assist business processes. Established hybrid platforms and reusing legacy systems as well as improve IT infrastructure management efficiency.

<<<
--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/intro-marketectures/intelligent-auto-claim-marketing-slide.png[750,700]
--

== The technology
--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/logical-diagrams/intelligent-auto-claim-ld-detail.png[750, 700]
--

IBM and Red Hat together provide cutting-edge technologies to facilitate and accelerate digital transformation with intelligent solutions in hybrid and multi-cloud environments. In this study, the client achieved intelligent automated business processes using  IBM Cloud Pak® for Business Automation and  IBM Cloud Pak® for Integration deployed on Red Hat OpenShift on the IBM Cloud. Additionally, IBM Watson® services on the IBM Cloud infused AI/ML into the applications with conversational chatbots in the claims processing, leveraged AutoAI to determine the best models for scoring claims based on complexity and risk, automating decisions and processes, and optimizing adjuster's time. 


IT service management (ITSM) is introduced to establish processes and practices to optimize the use of the IT services. Combine with Automated processes using Red Hat Ansible to replace manual work with more compliant and effective operations, while reducing the labor cost, consistent and dynamic adjustment, and enforcing guardrails to deliver infrastructure service increases agility allowing enterprise to scale to meet growth. 



The following technology was chosen for this solution:

====
*Red Hat OpenShift* Kubernetes offering, the hybrid platform offering allow deployment across data centers,
private and public clouds as it brings choices and flexible for hosting system and services.

*Red Hat Ansible Automation Platform* operate, scale and delegate automate IT services, track changes an update inventory, prevent configuration drift and  integrated with ITSM.  

*Business Automation Workflow* automate business processes, case work, task automation with Robotic Process Automation (RPA) and Intelligent Automation such as conversation intelligence. 

*IBM Cloud Pak for Data* unify and simplify the collection, organization and analysis of data. 
====


== Intelligent, smart automation for claims(AI/ML workflow)
--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/schematic-diagrams/intelligent-auto-claim-sd-aiml-workflow.png[750,700]
--

There are two personas supported in this schematic:
 
The *Customer / Claimant* high-level steps of this intelligent claims processing are:

. Customer / Claimant may use their mobile device to file a claim through a conversational (chatbot) implemented as Intelligent Virtual Agent implemented with IBM Watson Assistant and its associated Data Science Tooling. The Orchestrator (microservice) is initiated by this chatbot UI in the mobile device.
. The Orchestrator uses Digital Workers that can take actions on behalf of the human workers and thus off-load manual and repetitive work of the humans, such as:
Retrieving Weather data based on the accident location, date, and time
* Uploading pictures submitted by users in the mobile device to Image Recognition service
* Invoke Machine Learning (ML) to score / triage these claims as: Low, Medium, and Complex. This ML was implemented as an IBM Auto Claims Routing Accelerator developed with Data Science Tooling in IBM Watson Studio and AutoAI capabilities.
. Digital Workers update the Claims Service (microservice) that in turn, leverage integrations with the Legacy Claims systems and new Cloud-native applications. They can also implement Event Streaming Architectures – when events are coming in from chatbot, claims systems, underwriting, etc.
 
The *Insurer End User such as Adjuster / CSR / Admin* high-level steps of this intelligent claims processing are:

. Their entry point is the Claims Admin Dashboard web app that interacts through the Claims Service and similar technologies and integrations as the end user flow
. The Claims Admin Dashboard interacts with a Message consumer (IBM Event Streams, Red Hat Streams, Kafka) that implements the Event Streaming Architecture
. Similarly, the Claims Service interacts with a Dashboard message consumer (IBM Event Streams, Red Hat Streams, Kafka) that implements the Event Streaming Architecture




== Co-existing with Legacy and Accelerate with New
--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/schematic-diagrams/intelligent-auto-claim-sd-strangler-pattern.png[750,700]
--

A. New process can be accessed via API  endpoint, which is managed by an API management platform for security and access control. The new process is a consolidation of new and existing processes (hence this is the interface of the strangler pattern.). When the process kick starts, it will call the other tasks, service and process through using API calls or through events.

B. The digital worker represents an activity. Depending on the needs, activities can be automated with AI where the decision or operation is done by the predetermined data model, a third party service from vendors or existing processes. When applying the strangler pattern, we can simply reuse the legacy processes and replace it with new AI enabled ones. 

C. Utilizing past investment by calling pre-existing processes. A common problem with calling the existing service or process is translating data formats. A connector here helps transform data input and output to the receiver and requester. The processes are often built in a closed system, to access it, using API or REST endpoints are commonly found, sometimes with much older systems, there might be a need to use a special connector for its protocols. 

D. A big part of needing to transform digitally is taking advantage of AI, the digital worker automatically requests predictions/decisions from a machine learning model from the machine learning platform. 

E. Customizing services for specific enterprise needs, are common and implemented with microservices. Digital work can call either a single service or orchestrated services.   

F. A unify dashboard is created for seamless user experience and enhanced real-time features were provided to customers via website and Apps. Open API give partner availability to amplify and provide better services. 


<<<
== Operation efficiency 
--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/schematic-diagrams/intelligent-auto-claim-sd-itsm.png[750,700]
--

A. All infrastructure setup and configurations are stored as code using ansible playbooks, they are stored in a git repository for version control. (IaaS)

B. IT service management (ITSM) system is used to manage & support policies and processes for the IT services. Users can place their request which is automated to kick off a workflow or playbooks, where it will start provision/update or delete the target resource on prem or on cloud. Connected via API. 

C. Credentials and configuration are securely stored in the controller(can be configured to point elsewhere), which can be accessed when executing the playbooks. The automation does not end at the target, follow-ups such as scanning, syncing inventory, starting another business process, adding monitoring pieces and other configuration management, can run concurrently or sequentially with role-based controls. 

D. The targets to be created can be any resources on the cloud or in data centers.  Such as kubernete cluster, storage, VM, and network stacks. If a problem or failure occurs during execution, an incident will be created and sync back to ITSM for further tracking or reviewing. Additional components such as smart management were included as part of initial installation to add an extra layer of automated self-healing with incidents also recorded and sent back to ITSM. 

E. ServiceNow configuration management database and Ansible repository are in-sync with two way communication, this can keep accurate records of assets across multi-domain, disparate users, and teams. So all infrastructure information is up to date. 


== Download diagrams
View and download all of the diagrams above in our open source tooling site.
--
https://www.redhat.com/architect/portfolio/tool/index.html?#gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/diagrams/intelligent-auto-claim.drawio[[Open Diagrams]]
--

== Provide feedback on this architecture
You can offer to help correct or enhance this architecture by filing an https://gitlab.com/osspa/portfolio-architecture-examples/-/blob/main/intelligent-auto-claim.adoc[issue or submitting a merge request against this Portfolio Architecture product in our GitLab repositories].